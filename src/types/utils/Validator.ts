
// TODO write tests for the whole file

const email = (email: string): boolean => {
  const stringPattern = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

  return stringPattern.test(email);
};

const password = (pwd: string): boolean => {
  // A password should contain/be at least:
  // - a upper case character
  const hasUpperCase = pwd.search(/[A-Z]/) > -1;
  // - a number
  const hasNumber = pwd.search(/[0-9]/) > -1;
  // - 8 characters long
  const isAtLeast8Long = pwd.length >= 8;

  return hasUpperCase && hasNumber && isAtLeast8Long;
};

const tag = (label: string = ""): boolean => {
  // A tag should not contain/be:
  // - empty
  const isBlank = label === "";
  // - a carriage return
  const containCarriageReturn = label.search(/\r/) > -1;
  // - a tab
  const containTab = label.search(/\t/) > -1;
  // - more than 50 chars long
  const tooLong = label.length > 50;

  return !isBlank  && !containCarriageReturn && !containTab && !tooLong;
};

export {
  email,
  password,
  tag,
};