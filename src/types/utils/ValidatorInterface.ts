namespace Validation {
  interface Validation {
    (e: any): void;
    // new(element: any);
    isValid(element: any): boolean;
  }
  export class StringValidation {
    // constructor(super());
    isValid(chars: string): boolean {
      const stringPattern = /[a-zA-Z]+/g;

      return stringPattern.test(chars);
    }
  }
  export class NumberValidation {
    static isValid(number: string): boolean {
      const stringPattern = /[0-9]+/g;

      return stringPattern.test(number);
    }
  }

  export class EmailValidation {
    static isValid(email: string): boolean {
      const stringPattern = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

      return stringPattern.test(email);
    }
  }

  export class Validator {

    constructor() {

    }

    // validate(element: any, validationList: Array<string>): boolean {
    //   validationList.every((validator) => {
    //     return validator.isValid(element);
    //   });
    //   return;
    // }

  }

}