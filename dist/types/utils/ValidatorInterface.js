var Validation;
(function (Validation) {
    class StringValidation {
        // constructor(super());
        isValid(chars) {
            const stringPattern = /[a-zA-Z]+/g;
            return stringPattern.test(chars);
        }
    }
    Validation.StringValidation = StringValidation;
    class NumberValidation {
        static isValid(number) {
            const stringPattern = /[0-9]+/g;
            return stringPattern.test(number);
        }
    }
    Validation.NumberValidation = NumberValidation;
    class EmailValidation {
        static isValid(email) {
            const stringPattern = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
            return stringPattern.test(email);
        }
    }
    Validation.EmailValidation = EmailValidation;
    class Validator {
        constructor() {
        }
    }
    Validation.Validator = Validator;
})(Validation || (Validation = {}));
//# sourceMappingURL=ValidatorInterface.js.map