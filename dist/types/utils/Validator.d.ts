declare const email: (email: string) => boolean;
declare const password: (pwd: string) => boolean;
declare const tag: (label?: string) => boolean;
export { email, password, tag, };
