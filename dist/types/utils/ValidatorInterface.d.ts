declare namespace Validation {
    class StringValidation {
        isValid(chars: string): boolean;
    }
    class NumberValidation {
        static isValid(number: string): boolean;
    }
    class EmailValidation {
        static isValid(email: string): boolean;
    }
    class Validator {
        constructor();
    }
}
