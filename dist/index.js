"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
// import * as TextFormatter from "./types/utils/TextFormatter";
const Validator = __importStar(require("./types/utils/Validator"));
exports.Validator = Validator;
//# sourceMappingURL=index.js.map